import { Session } from '../../../../lib/interfaces/Session';
import { Database } from '../../../../lib/lambda/middleware/withDatabase';

// This file is ignored for test coverage in the Jest configuration
// since it is merely a translation of Javascript objects to SQL queries.
export async function addHeaderImageToPeriodical(
  database: Database,
  session: Session,
  periodicalId: string,
  imageUrl: string,
): Promise<null> {
  return database.none(
    'UPDATE periodicals'
    // tslint:disable-next-line:no-invalid-template-strings
    + ' SET image=${imageUrl}'
    + ' WHERE uuid='
    + ' ('
      + 'SELECT uuid FROM periodicals p'
      + ' LEFT JOIN periodical_creators pc ON pc.periodical=p.uuid'
      + ' WHERE '
        // tslint:disable-next-line:no-invalid-template-strings
        + 'p.identifier=${periodicalId}'
        // tslint:disable-next-line:no-invalid-template-strings
        + ' AND (p.creator_session=${sessionId} OR pc.creator=${creatorId})'
      + ')',
    {
      creatorId: (session.account) ? session.account.identifier : undefined,
      imageUrl,
      periodicalId,
      sessionId: session.identifier,
    },
  );
}
