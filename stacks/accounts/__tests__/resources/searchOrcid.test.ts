jest.mock(
  'node-fetch',
  () => ({
    default: jest.fn().mockReturnValue(Promise.resolve({
      json: () => Promise.resolve({ result: [
        { 'orcid-identifier': { path: 'arbitrary-orcid' } },
      ] }),
    })),
  }),
);

import { searchOrcid } from '../../src/resources/searchOrcid';

const mockContext = {
  body: {},

  headers: {},
  method: 'GET' as 'GET',
  params: [ '/orcid/search', 'arbitrary_query' ],
  path: '/orcid/search/arbitrary_query',
  query: null,
};

beforeEach(() => {
  process.env.orcid_base_path = 'https://arbitrary.orcid.basepath';
});

it('should return the ORCID response when passed the correct values', () => {
  return expect(searchOrcid(mockContext)).resolves.toHaveProperty('length', 1);
});

it('should fetch the details of every search result', (done) => {
  const mockedFetch = require.requireMock('node-fetch').default;
  mockedFetch.mockReturnValueOnce(Promise.resolve({
      json: () => Promise.resolve({ result: [
        { 'orcid-identifier': { path: 'first-orcid' } },
        { 'orcid-identifier': { path: 'second-orcid' } },
      ] }),
    }),
  );

  searchOrcid(mockContext);

  setImmediate(() => {
    expect(mockedFetch.mock.calls.length).toBe(3);
    expect(mockedFetch.mock.calls[1][0]).toMatch('first-orcid');
    expect(mockedFetch.mock.calls[2][0]).toMatch('second-orcid');

    done();
  });
});

it('should error when no search terms were specified', () => {
  return expect(searchOrcid({
    ...mockContext,
    params: [ '/orcid/search' ],
    path: '/orcid/search',
  })).rejects
    .toEqual(new Error('Please specify a search query.'));
});

it('should error when the ORCID API returns an error', () => {
  const mockedFetch = require.requireMock('node-fetch').default.mockReturnValueOnce(
    Promise.resolve({ json: () => Promise.resolve({ error: 'Arbitrary error' }) }),
  );

  return expect(searchOrcid(mockContext)).rejects
    .toEqual(new Error('Could not fetch ORCID search results, please try again.'));
});

it('should call to the sandbox public API when the member API is also pointing to the sandbox', () => {
  const mockedFetch = require.requireMock('node-fetch').default;
  process.env.orcid_base_path = 'https://arbitrary.sandbox.orcid.org.basepath';

  searchOrcid(mockContext);

  expect(mockedFetch.mock.calls[0][0]).toMatch('.sandbox');
});
